# React + Webpack Starter

This is a tweaked version of the Yo generator, named generator-react-webpack.

The purpose of this project is to have a foundation on which to build React apps. After spending hours of configuring a starter from scratch, Yo came to the rescue.  After evaluating several Yo generators for React-based projects, the generator-react-webpack was picked.

### Additions

The following extras were added to this stater:

- Bootstrap 4 alpha.2
- jQuery 2
- Font-awesome 4.5

### How to use

`npm run serve` to use localhost dev server including browser refresh and hotloading

`npm run dist` to build/transpile the source directory files into /dist folder

`npm run serve:dist` to run a server using the /dist folder contents
