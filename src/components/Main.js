require('normalize.css');
require('styles/main.scss');
require('bootstrap/dist/css/bootstrap.css');

//var $ = require('jquery');

import React from 'react';

let yeomanImage = require('../images/yeoman.png');

class AppComponent extends React.Component {
  render() {
    return (
      <div className="index">
        <div className="container">
          <div className="jumbotron">
            <h1 className="text-center">My React App</h1>
            <p className="text-center">This is a React component</p>
            <img src={yeomanImage} alt="Yeoman Generator" />
            <p><i className="fa fa-3x fa-thumbs-up"></i></p>
          </div>
        </div>
        <div className="container-fluid notice">Please edit <code>src/components/Main.js</code> to get started!</div>
      </div>
    );
  }
}

AppComponent.defaultProps = {
};

export default AppComponent;
